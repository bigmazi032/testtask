package ru.zinyakov.logcounter;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import static java.nio.file.StandardOpenOption.CREATE;


public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Требуется задать каталог с log файлами!");
            System.exit(0);
        }
        long startTime = System.currentTimeMillis();


        String pathIn = args[0];
        String pathOut = pathIn + "\\Statistics.txt";

        List<ErrorCounter> errorCounters = new ArrayList<>();

        ErrorStat es = new ErrorStat();

        try {
            List<Path> paths = listSourceFiles(Paths.get(pathIn));
            for (Path p : paths) {

                errorCounters.add(new ErrorCounterMMF(p.toString(), es));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (ErrorCounter ec : errorCounters) {
            try {
                ec.getThread().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        saveStatistic(pathOut, es.toList());
        System.out.println("Найдено ошибок типа ERROR = " + es.getCounter());
        System.out.println("Время выполнения программы = "+ (System.currentTimeMillis() - startTime));
        System.out.println("Результаты сохранены в файл = " + pathOut);
    }


    private static List<Path> listSourceFiles(Path dir) throws IOException {
        List<Path> result = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.log.*")) {
            for (Path entry : stream) {
                result.add(entry);
            }
        } catch (DirectoryIteratorException ex) {

            throw ex.getCause();
        }
        return result;
    }

    private static void saveStatistic(String pathOut, List<String> strings) {

        Path path = Paths.get(pathOut);

        try (PrintStream ps = new PrintStream(new BufferedOutputStream(Files.newOutputStream(path, CREATE)))) {
            for (String s : strings) {
                ps.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
