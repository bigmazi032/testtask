package ru.zinyakov.logcounter;

public interface ErrorCounter {
    Thread getThread ();
}
