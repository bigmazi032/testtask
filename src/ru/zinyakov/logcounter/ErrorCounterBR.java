package ru.zinyakov.logcounter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;



public class ErrorCounterBR implements Runnable, ErrorCounter {

    private String path;
    private ErrorStat es;
    private Thread t;


    public ErrorCounterBR(String path, ErrorStat es){
        this.path = path;
        this.es = es;
        this.t = new Thread(this);
        this.t.start();

    }

    @Override
    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader((new FileInputStream(path))));

            while (br.ready()) {
                String s = br.readLine();
                if (isError(s)) {
                    es.countLog(getTypeException(s));

                }

            }
        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    private boolean isError (String s) {

        return s.contains("ERROR");
    }

    private String getTypeException(String s){
        return s.substring(26, s.indexOf(" ", 28));
    }

@Override
    public Thread getThread(){
        return this.t;
    }

}
