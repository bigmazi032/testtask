package ru.zinyakov.logcounter;

import java.util.*;

public class ErrorStat {
    private Map<String, Integer> mapOfLog = new TreeMap<>();

    private int counter;

    synchronized void countLog(String log) {
        mapOfLog.compute(log, (a, b) -> makeCalculation(b));
        counter++;

    }


    private Integer makeCalculation(Integer i) {
        if (i == null)
            return 1;
        return ++i;
    }


    public int getCounter() {
        return counter;
    }



    public List<String> toList() {
        List<String> logs = new ArrayList<>();
        mapOfLog.forEach((a, b) -> logs.add(a + " = " + b));
        return logs;
    }
}
