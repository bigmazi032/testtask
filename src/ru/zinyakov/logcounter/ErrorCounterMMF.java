package ru.zinyakov.logcounter;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;


public class ErrorCounterMMF implements Runnable, ErrorCounter {

    private String path;
    private ErrorStat es;
    private Thread t;


    public ErrorCounterMMF(String path, ErrorStat es) {
        this.path = path;
        this.es = es;
        this.t = new Thread(this);
        this.t.start();

    }
@Override
    public Thread getThread(){
        return this.t;
    }

    @Override
    public void run() {
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            MappedByteBuffer mbb = file.getChannel()
                    .map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            StringBuilder sb = new StringBuilder();
            while (mbb.hasRemaining()) {

                if ((char) mbb.get() == 'E') {
                    if ((char) mbb.get() == 'R') {
                        if ((char) mbb.get() == 'R') {
                            if ((char) mbb.get() == 'O') {
                                if ((char) mbb.get() == 'R') {
                                    mbb.get();

                                    boolean stop = false;
                                    do {
                                        byte b = mbb.get();
                                        if ((char) b==' ') {
                                            stop =true;
                                        }
                                        sb.append((char)b);
                                    } while (!stop);
                                    es.countLog(sb.toString());
                                    sb.delete(0,sb.length());
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();

        }

    }

}


